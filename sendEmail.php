<?php
error_reporting(E_ALL & ~E_NOTICE);
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once 'views/header.php';
require_once 'db.php';
require_once 'conn.php';

//get username from request
if (!empty($_SESSION['username'])) {
    $member = $_GET['username'];
} else {
    //Redirect to showMembers
    header("Location: http://localhost:63342/workspace/ex_ex/showMembers.php?order=ASC");
    exit();
}

function validate_subject($field)
{
    $status = true;
    $message = "";
    if ($field == "") {
        $status = false;
        $message = 'No subject was entered';
    } else if (strlen($field) < 2) {
        $status = false;
        $message = 'Subject must be at least 2 characters';
    }

    return [
        'status' => $status,
        'message' => $message
    ];
}

function validate_message($field)
{
    $status = true;
    $message = "";
    if ($field == "") {
        $status = false;
        $message = 'No message was entered';
    } else if (strlen($field) < 2) {
        $status = false;
        $message = 'Message must be at least 2 characters';
    }

    return [
        'status' => $status,
        'message' => $message
    ];
}

function fix_string($string)
{
    if (get_magic_quotes_gpc()) {
        $string = stripslashes($string);
    }
    return htmlentities($string);
}

function fix_variable($form_vars)
{
    if (isset($_POST['subject']))
        $form_vars['subject'] = fix_string($_POST['subject']);
    if (isset($_POST['message']))
        $form_vars['message'] = fix_string($_POST['message']);

    return $form_vars;
}

function validate($form_vars)
{
    $validators['subject'] = validate_subject($form_vars['subject']);
    $validators['message'] = validate_message($form_vars['message']);

    return $validators;
}

function validate_entire_form($validators)
{
    $fail = $validators['subject']['message'] .
        $validators['message']['message'];

    if ($fail == '') {
        return "Send form data successfully validated";
    } else {
        return "Send form data not successfully validated";
    }
}

function import_email_adress($conn, $member)
{
//connect and select database
    $database = "userex";
    $sql = "USE $database";
    $result = mysqli_query($conn, $sql);

//show user from table
    $table = "users";

    $sql = "SELECT email FROM $table  WHERE username = '$member'";
    $result = mysqli_query($conn, $sql);

//prepare the data
    $userEmail = "";

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();

        $userEmail = $row['email'];

        return $userEmail;
    }
}

//get email adress from database
$userEmailToSend = import_email_adress($conn, $member);

//get sender email adress from database
$sesionUsername = $_SESSION['username'];
$senderEmailAdress = import_email_adress($conn, $sesionUsername);

function send_email($to, $from, $form_vars)
{
    //prepare data

    $footerMessage = "<br>Send from My Aplication";
    $finalMessage = $form_vars['message'] . $footerMessage;

    $headers = "From: $from";
    $subject = $form_vars['subject'];

    if (mail($to, $subject, $finalMessage, $headers)) {
        return true;
    }
}

if (isset($_POST["submit"])) {
    $form_vars = [
        'subject' => '',
        'message' => '',
    ];

    $form_vars = fix_variable($form_vars);
    $validators = validate($form_vars);

    $general_message = validate_entire_form($validators);

    if ($general_message == "Send form data successfully validated") {
        echo "<script>alert('Succes validation');</script>";

        if (send_email($userEmailToSend, $senderEmailAdress, $form_vars)) {
            echo "Mail send";
            //Redirect to showMembers
            header("Location: http://localhost:63342/workspace/ex_ex/showMembers.php?order=ASC");
            exit();
        } else {
            echo "Unable to send email";
        }
    }
}
require_once 'views/sendEmailTable.php';

