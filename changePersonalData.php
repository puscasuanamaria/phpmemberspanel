<?php
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}
require_once 'views/header.php';
require_once 'db.php';
require_once 'conn.php';

function import_old_details_sql($conn)
{
    //get user name from sesion
    $user = $_SESSION['username'];

    //connect and select database
    $database = "userex";
    $sql = "USE $database";
    mysqli_query($conn, $sql);

    //get all old informations about user
    $table = "users";

    $sql = "SELECT name, username, email, gender, civil_status, extension FROM $table WHERE username = '$user'";
    $result = mysqli_query($conn, $sql);

    //prepare old information array
    $old_informations = [
        'fname' => '',
        'uname' => '',
        'email' => '',
        'gender' => '',
        'civil' => '',
        'extension' => ''
    ];

    //set old informations
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();

        $old_informations['fname'] = $row['name'];
        $old_informations['uname'] = $row['username'];
        $old_informations['email'] = $row['email'];
        $old_informations['gender'] = $row['gender'];
        $old_informations['civil'] = $row['civil_status'];
        $old_informations['extension'] = $row['extension'];

        return $old_informations;

    } else {
        echo "Username does not exist in database";
    }
}

function validate_fname($field)
{
    $status = true;
    $message = "";
    if ($field == "") {
        $status = false;
        $message = 'No full name was entered';
    }
    return [
        'status' => $status,
        'message' => $message
    ];
}

function validate_uname($field, $conn, $old_informations)
{
    $status = true;
    $message = "";

    if ($field == "") {
        $status = false;
        $message = 'No username was entered';
    } else if (strlen($field) < 5) {
        $status = false;
        $message = 'Usernames must be at least 5 characters';
    } else if (preg_match("/[^a-zA-Z0-9_-]/", $field)) {
        $status = false;
        $message = 'Only letters, numbers, - and _ in usernames';
    } else if (validate_uname_database($field, $conn, $old_informations)) {
        $status = false;
        $message = 'Username exists.';
    }

    return [
        'status' => $status,
        'message' => $message
    ];

}

function validate_uname_database($field, $conn, $old_informations)
{

    $status = false;

    //prepare data before check
    // mysql_real_escape_string
    $field = trim(strip_tags($field));

    if ($old_informations['uname'] != $field) {
        //connect and select database
        $database = "userex";
        $sql = "USE $database";
        $result = mysqli_query($conn, $sql);

        //check if username exists
        $table = "users";
        $sql = "SELECT username FROM $table WHERE username = '$field'";
        $result = mysqli_query($conn, $sql);

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $status = true;
        }
    }

    return $status;
}

function validate_email($field, $conn, $old_informations)
{
    $status = true;
    $message = "";
    if ($field == "") {
        $status = false;
        $message = 'No email was entered';
    } else if (!((strpos($field, ".") > 0) &&
            (strpos($field, "@") > 0)) ||
        preg_match("/[^a-zA-Z0-9.@_-]/", $field)) {
        $status = false;
        $message = 'The Email address is invalid';
    } else if (validate_email_database($field, $conn, $old_informations)) {
        $status = false;
        $message = 'Email exists.';
    }

    return [
        'status' => $status,
        'message' => $message
    ];
}

function validate_email_database($field, $conn, $old_informations)
{

    $status = false;

    //prepare data before check
    // mysql_real_escape_string
    $field = trim(strip_tags($field));

    if ($old_informations['email'] != $field) {
        //connect and select database
        $database = "userex";
        $sql = "USE $database";
        $result = mysqli_query($conn, $sql);

        //check if username exists
        $table = "users";
        $sql = "SELECT email FROM $table WHERE email = '$field'";
        $result = mysqli_query($conn, $sql);

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $status = true;
        }
    }

    return $status;
}

function fix_string($string)
{
    if (get_magic_quotes_gpc()) {
        $string = stripslashes($string);
    }
    return htmlentities($string);
}

function validate_photo()
{
    $status = true;
    $message = "";

    //var_dump($_FILES["fileToUpload"]);

    if ($_FILES["fileToUpload"]["name"] != "") {

        $target_dir = "./uploads/";
        $target_file = basename($_FILES["fileToUpload"]["name"]);
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        $fileName = $target_dir . $_POST["uname"] . "." . $imageFileType;

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if ($check == false) {
            $message = "File is not an image.";
            $status = false;
        }

//        // Check if file already exists
//        if (file_exists($fileName)) {
//            $message = "Sorry, file already exists.";
//            $status = false;
//        }

        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            $message = "Sorry, your file is too large.";
            $status = false;
        }

        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif") {
            $message = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $status = false;
        }
    }

    return [
        'status' => $status,
        'message' => $message
    ];
}

function upload_photo()
{
    $target_dir = "./uploads/";
    $target_file = basename($_FILES["fileToUpload"]["name"]);

    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    $fileName = $target_dir . $_POST["uname"] . "." . $imageFileType;

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $fileName)) {
        return "";
    } else {
        return "Sorry, there was an error uploading your file.";
    }
}

function fix_variable($form_vars)
{
    if (isset($_POST['fname'])) {
        $capitalisedFname = ucwords(strtolower($_POST['fname']));
        $form_vars['fname'] = fix_string($capitalisedFname);
    }
    if (isset($_POST['uname']))
        $form_vars['uname'] = fix_string($_POST['uname']);
    if (isset($_POST['email']))
        $form_vars['email'] = fix_string($_POST['email']);
    if (isset($_POST['gender']))
        $form_vars['gender'] = fix_string($_POST['gender']);
    if (isset($_POST['civil']))
        $form_vars['civil'] = fix_string($_POST['civil']);

    return $form_vars;
}

function validate($form_vars, $conn, $old_informations)
{
    $validators['fname'] = validate_fname($form_vars['fname']);
    $validators['uname'] = validate_uname($form_vars['uname'], $conn, $old_informations);
    $validators['email'] = validate_email($form_vars['email'], $conn, $old_informations);
    $validators['upload_img'] = validate_photo();

    return $validators;
}

function validate_entire_form($validators, $old_informations)
{
    $fail = $validators['fname']['message'] .
        $validators['uname']['message'] .
        $validators['email']['message'] .
        $validators['upload_img']['message'];


    if ($fail == '') {

        $upload_error = "";

        if ($_FILES["fileToUpload"]["name"] != "") {
            $upload_error = upload_photo();
        } else {
            $target_dir = "./uploads/";

            $imageFileType = $old_informations['extension'];
            $newFileName = $target_dir . $_POST["uname"] . "." . $imageFileType;
            $oldFileName = $target_dir . $_SESSION['username'] . "." . $imageFileType;
//            todo: copy does not has permissions
//            copy($oldFileName, $newFileName);
//            unlink($oldFileName);
        }

        if ($upload_error == "")
            return "Form data successfully validated";
        else
            return $upload_error;
    } else {
        return "Form data not successfully validated";
    }
}

function update_sql($conn, $form_vars)
{
    //connect and select database
    $database = "userex";
    $sql = "USE $database";
    $result = mysqli_query($conn, $sql);

    $table = "users";

    //update table - prepare statement
    $stmt = $conn->prepare("UPDATE $table SET name=?, username=?, email=?, gender=?, civil_status=?, extension=? WHERE username=?");

    //prepare data before insert into table
    // mysql_real_escape_string
    $name = trim(strip_tags($form_vars['fname']));
    $username = trim(strip_tags($form_vars['uname']));
    $email = trim(strip_tags($form_vars['email']));
    $gender = isset($form_vars['gender']) ? $form_vars['gender'] : "";
    $civil_status = isset($form_vars['civil']) ? $form_vars['civil'] : "";
    $target_file = basename($_FILES["fileToUpload"]["name"]);
    $extension = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    //get user name from sesion
    $user = $_SESSION['username'];

    $stmt->bind_param("sssssss", $name, $username, $email, $gender, $civil_status, $extension, $user);
    $stmt->execute();
}

//the main logic

//get all informations from database
$old_informations = import_old_details_sql($conn);

if (isset($_POST["submit"])) {
    $form_vars = [
        'fname' => '',
        'uname' => '',
        'email' => '',
        'gender' => '',
        'civil' => ''
    ];

    $form_vars = fix_variable($form_vars);
    $validators = validate($form_vars, $conn, $old_informations);

    $general_message = validate_entire_form($validators, $old_informations);

    if ($general_message == "Form data successfully validated") {
        update_sql($conn, $form_vars);
        $_SESSION["username"] = $form_vars['uname'];

        echo "<script>alert('Succes registration');</script>";
    }
}
require_once 'views/changePersonalDataForm.php';
