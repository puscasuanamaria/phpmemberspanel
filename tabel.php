<?php
require_once 'conn.php';
$database = "userex";


$sql = "USE $database";
$result = mysqli_query($conn, $sql);

$table = "users";
$sql = "CREATE TABLE IF NOT EXISTS $table( 
            id INT AUTO_INCREMENT PRIMARY KEY, 
            name VARCHAR(50), 
            username VARCHAR(50) UNIQUE KEY, 
            email VARCHAR(50) UNIQUE KEY, 
            password VARCHAR (256), 
            gender ENUM('male', 'female'), 
            civil_status ENUM('married', 'single', 'divorced'),
            date DATE,
            extension VARCHAR(10)
        )";

$result = mysqli_query($conn, $sql);

$sql = "ALTER TABLE $table ADD phone_no VARCHAR(13) AFTER email";
$result = mysqli_query($conn, $sql);


$table = "users_friends";
$sql = "CREATE TABLE IF NOT EXISTS $table( 
            owner_username VARCHAR(50), 
            friend_username VARCHAR(50), 
            CONSTRAINT PK_Person PRIMARY KEY (owner_username,friend_username)
        )";

$result = mysqli_query($conn, $sql);