<?php
require_once 'db.php';

//waiting for the ajax request
$str_json = file_get_contents('php://input');

//decode the request parameters
$str_json = json_decode($str_json);

//make database request

//connect and select database
$database = "userex";
$sql = "USE $database";
$result = mysqli_query($conn, $sql);

//check if username exists
$response = false;

$table = "users";
$sql = "SELECT username FROM $table WHERE username = '$str_json'";
$result = mysqli_query($conn, $sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();

    if ($str_json == $row['username']) {
        $response = true;
    } else {
        $response = false;
    }
} else {
    $response = false;
}

//send the response
echo json_encode($response);