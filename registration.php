<?php
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}
require_once 'views/header.php';
require_once 'db.php';

function validate_fname($field)
{
    $status = true;
    $message = "";
    if ($field == "") {
        $status = false;
        $message = 'No full name was entered';
    }
    return [
        'status' => $status,
        'message' => $message
    ];
}

function validate_uname($field, $conn)
{
    $status = true;
    $message = "";

    if ($field == "") {
        $status = false;
        $message = 'No username was entered';
    } else if (strlen($field) < 5) {
        $status = false;
        $message = 'Usernames must be at least 5 characters';
    } else if (preg_match("/[^a-zA-Z0-9_-]/", $field)) {
        $status = false;
        $message = 'Only letters, numbers, - and _ in usernames';
    } else if (validate_uname_database($field, $conn)) {
        $status = false;
        $message = 'Username exists.';
    }

    return [
        'status' => $status,
        'message' => $message
    ];

}

function validate_uname_database($field, $conn)
{

    $status = false;

    //prepare data before check
    // mysql_real_escape_string
    $field = trim(strip_tags($field));

    //connect and select database
    $database = "userex";
    $sql = "USE $database";
    $result = mysqli_query($conn, $sql);

    //check if username exists
    $table = "users";
    $sql = "SELECT username FROM $table WHERE username = '$field'";
    $result = mysqli_query($conn, $sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $status = true;
    }

    return $status;
}

function validate_email($field, $conn)
{
    $status = true;
    $message = "";
    if ($field == "") {
        $status = false;
        $message = 'No email was entered';
    } else if (!((strpos($field, ".") > 0) &&
            (strpos($field, "@") > 0)) ||
        preg_match("/[^a-zA-Z0-9.@_-]/", $field)) {
        $status = false;
        $message = 'The Email address is invalid';
    } else if (validate_email_database($field, $conn)) {
        $status = false;
        $message = 'Email exists.';
    }

    return [
        'status' => $status,
        'message' => $message
    ];
}

function validate_email_database($field, $conn)
{

    $status = false;

    //prepare data before check
    // mysql_real_escape_string
    $field = trim(strip_tags($field));

    //connect and select database
    $database = "userex";
    $sql = "USE $database";
    $result = mysqli_query($conn, $sql);

    //check if username exists
    $table = "users";
    $sql = "SELECT email FROM $table WHERE email = '$field'";
    $result = mysqli_query($conn, $sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $status = true;

    }

    return $status;
}

function validate_phone($field)
{
    $status = true;
    $message = "";
    if ($field == "") {
        $status = false;
        $message = 'No phone number was entered';
    } else if (!preg_match("/^\+?([0-9]{2})\)?[ ]?([0-9]{4})[ ]?([0-9]{4})$/", $field)) {
        $status = false;
        $message = 'The phone is invalid';

        return [
            'status' => $status,
            'message' => $message
        ];
    }
}

function validate_password($field)
{
    $status = true;
    $message = "";
    if ($field == "") {
        $status = false;
        $message = 'No password was entered';
    } else if (strlen($field) < 6) {
        $status = false;
        $message = 'Passwords must be at least 6 characters';
    } else if (!preg_match("/[a-z]/", $field) ||
        !preg_match("/[A-Z]/", $field) ||
        !preg_match("/[0-9]/", $field)) {
        $status = false;
        $message = 'Passwords require 1 each of a-z, A-Z and 0-9';
    }

    return [
        'status' => $status,
        'message' => $message
    ];
}

function validate_passwords_are_equal($password, $rpassword)
{
    $status = true;
    $message = "";
    if ($password !== $rpassword) {
        $status = false;
        $message = 'The passwords are not equal';
    }

    return [
        'status' => $status,
        'message' => $message
    ];
}

function fix_string($string)
{
    if (get_magic_quotes_gpc()) {
        $string = stripslashes($string);
    }
    return htmlentities($string);
}

function validate_photo()
{
    $status = true;
    $message = "";

    //var_dump($_FILES["fileToUpload"]);

    if ($_FILES["fileToUpload"]["name"] != "") {

        $target_dir = "./uploads/";
        $target_file = basename($_FILES["fileToUpload"]["name"]);
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        $fileName = $target_dir . $_POST["uname"] . "." . $imageFileType;

        // Check if image file is a actual image or fake image
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if ($check == false) {
            $message = "File is not an image.";
            $status = false;
        }

        // Check if file already exists
        if (file_exists($fileName)) {
            $message = "Sorry, file already exists.";
            $status = false;
        }

        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            $message = "Sorry, your file is too large.";
            $status = false;
        }

        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif") {
            $message = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $status = false;
        }
    } else {
        $status = false;
        $message = "Please choose an image to upload.";
    }

    return [
        'status' => $status,
        'message' => $message
    ];
}

function upload_photo()
{
    $target_dir = "./uploads/";
    $target_file = basename($_FILES["fileToUpload"]["name"]);

    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    $fileName = $target_dir . $_POST["uname"] . "." . $imageFileType;

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $fileName)) {
        return "";
    } else {
        return "Sorry, there was an error uploading your file.";
    }
}

function fix_variable($form_vars)
{
    if (isset($_POST['fname'])) {
        $capitalisedFname = ucwords(strtolower($_POST['fname']));
        $form_vars['fname'] = fix_string($capitalisedFname);
    }
    if (isset($_POST['uname']))
        $form_vars['uname'] = fix_string($_POST['uname']);
    if (isset($_POST['email']))
        $form_vars['email'] = fix_string($_POST['email']);
    if (isset($_POST['phone']))
        $form_vars['phone'] = fix_string($_POST['phone']);
    if (isset($_POST['password']))
        $form_vars['password'] = fix_string($_POST['password']);
    if (isset($_POST['rpassword']))
        $form_vars['rpassword'] = fix_string($_POST['rpassword']);
    if (isset($_POST['gender']))
        $form_vars['gender'] = fix_string($_POST['gender']);
    if (isset($_POST['civil']))
        $form_vars['civil'] = fix_string($_POST['civil']);

    return $form_vars;
}

function validate($form_vars, $conn)
{
    $validators['fname'] = validate_fname($form_vars['fname']);
    $validators['uname'] = validate_uname($form_vars['uname'], $conn);
    $validators['email'] = validate_email($form_vars['email'], $conn);
    $validators['phone'] = validate_phone($form_vars['phone']);
    $validators['password'] = validate_password($form_vars['password']);
    $validators['rpassword'] = validate_password($form_vars['rpassword']);
    $validators['password_equal'] = validate_passwords_are_equal($form_vars['password'], $form_vars['rpassword']);
    $validators['upload_img'] = validate_photo();

    return $validators;
}

function validate_entire_form($validators)
{

    $fail = $validators['fname']['message'] .
        $validators['uname']['message'] .
        $validators['email']['message'] .
        $validators['phone']['message'] .
        $validators['password']['message'] .
        $validators['rpassword']['message'] .
        $validators['password_equal']['message'] .
        $validators['upload_img']['message'];

    if ($fail == '') {

        $upload_error = upload_photo();

        if ($upload_error == "")
            return "Form data successfully validated";
        else
            return $upload_error;
    } else {
        return "Form data not successfully validated";
    }
}

function insert_sql($conn, $form_vars)
{
    //connect and select database
    $database = "userex";
    $sql = "USE $database";
    $result = mysqli_query($conn, $sql);

//    //insert into table
    $table = "users";
//    $sql = "INSERT INTO $table (name, username, email, password, gender, civil_status, date, extension)
//            VALUES ('$name', '$username', '$email', '$password', '$gender', '$civil_status', '$date', '$extension')";
//    $result = mysqli_query($conn, $sql);

    //insert into table - prepare statement
    $stmt = $conn->prepare("INSERT INTO $table (name, username, email, phone_no, password, gender, civil_status, date, extension)
                            VALUES (?, ?, ?, ?, ?, ?, ?,?,?)");

    //prepare data before insert into table
    // mysql_real_escape_string
    $name = trim(strip_tags($form_vars['fname']));
    $username = trim(strip_tags($form_vars['uname']));
    $email = trim(strip_tags($form_vars['email']));
    $phone = trim(strip_tags($form_vars['phone']));
    $password = password_hash($form_vars['password'], PASSWORD_DEFAULT);
    $gender = isset($form_vars['gender']) ? $form_vars['gender'] : "";
    $civil_status = isset($form_vars['civil']) ? $form_vars['civil'] : "";
    $date = "";
    $extension = "";
    $target_file = basename($_FILES["fileToUpload"]["name"]);
    $extension = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));


    $stmt->bind_param("sssssssss", $name, $username, $email, $phone, $password, $gender, $civil_status, $date, $extension);
    $stmt->execute();
}

function insert_textfile($form_vars)
{
    $filename = './resources/users.txt';
    $fp = fopen($filename, 'a+');

    fwrite($fp, $form_vars['fname'] . PHP_EOL);
    fclose($fp);
}

function insert_XML($form_vars)
{
    //create xml file
    $filename = './resources/users.xml';

    //Creates XML string and XML document using the DOM
    $dom = new DomDocument('1.0', 'UTF-8');

    //load data
    $dom->load($filename);

    //add root
    $users = $dom->appendChild($dom->createElement('users'));

    //add Node element to root
    $user = $dom->createElement('user');
    $users->appendChild($user);

    // Appending attributes to the node
    $attr = $dom->createAttribute('name');
    $attr->appendChild($dom->createTextNode($form_vars['fname']));
    $user->appendChild($attr);

    $dom->formatOutput = true;

    // save XML as string or file
    $userslist = $dom->saveXML(); // put string in test1
    $dom->save($filename); // save as file
}

function insert_JSON($form_vars)
{
    $filename = './resources/users.json';
    $arr_data = array(); // create empty array

    // Get the contents of the JSON file
    $jsondata = file_get_contents($filename);

    // converts json data into array
    $arr_data = json_decode($jsondata, true);
    if(is_null($arr_data)){
        $arr_data = array(); // create empty array
    }

    // Push user data to array
    array_push($arr_data, $form_vars['fname']);

    //Convert updated array to JSON
    $jsondata = json_encode($arr_data, JSON_PRETTY_PRINT);

    //write json data into data.json file
    if(file_put_contents($filename, $jsondata)) {
        echo 'Data successfully saved';
    }
    else
        echo "error";
}

if (isset($_POST["submit"])) {
    require_once 'conn.php';

    $conn = mysqli_connect($servername, $username, $password, $database);

    $form_vars = [
        'fname' => '',
        'uname' => '',
        'email' => '',
        'phone' => '',
        'password' => '',
        'rpassword' => '',
        'gender' => '',
        'civil' => ''
    ];

    $form_vars = fix_variable($form_vars);
    $validators = validate($form_vars, $conn);

    $general_message = validate_entire_form($validators);

    if ($general_message == "Form data successfully validated") {
        insert_sql($conn, $form_vars);
        insert_textfile($form_vars);
//        insert_XML($form_vars);
        insert_JSON($form_vars);

        echo "<script>alert('Succes registration');</script>";
    }
}

require_once 'views/registrationForm.php';