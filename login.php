<?php
if(session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}
require_once 'views/header.php';
require_once 'db.php';

function validate_uname($field)
{
    $status = true;
    $message = "";
    if ($field == "") {
        $status = false;
        $message = 'No username was entered';
    } else if (strlen($field) < 5) {
        $status = false;
        $message = 'Usernames must be at least 5 characters';
    } else if (preg_match("/[^a-zA-Z0-9_-]/", $field)) {
        $status = false;
        $message = 'Only letters, numbers, - and _ in usernames';
    }

    return [
        'status' => $status,
        'message' => $message
    ];
}

function validate_password($field)
{
    $status = true;
    $message = "";
    if ($field == "") {
        $status = false;
        $message = 'No password was entered';
    } else if (strlen($field) < 6) {
        $status = false;
        $message = 'Passwords must be at least 6 characters';
    } else if (!preg_match("/[a-z]/", $field) ||
        !preg_match("/[A-Z]/", $field) ||
        !preg_match("/[0-9]/", $field)) {
        $status = false;
        $message = 'Passwords require 1 each of a-z, A-Z and 0-9';
    }

    return [
        'status' => $status,
        'message' => $message
    ];
}

function fix_string($string)
{
    if (get_magic_quotes_gpc()) {
        $string = stripslashes($string);
    }
    return htmlentities($string);
}

function fix_variable($form_vars)
{
    if (isset($_POST['uname']))
        $form_vars['uname'] = fix_string($_POST['uname']);
    if (isset($_POST['password']))
        $form_vars['password'] = fix_string($_POST['password']);

    return $form_vars;
}

function validate($form_vars)
{
    $validators['uname'] = validate_uname($form_vars['uname']);
    $validators['password'] = validate_password($form_vars['password']);

    return $validators;
}

function validate_entire_form($validators)
{
    $fail = $validators['uname']['message'] .
        $validators['password']['message'];

    if ($fail == '') {
        return "Login data successfully validated";
    } else {
        return "Login data not successfully validated";
    }
}

function add_date($conn, $username){
    //connect and select database
    $database = "userex";
    $sql = "USE $database";
    $result = mysqli_query($conn, $sql);

    //update table
    $table = "users";

    //prepare data
    $date = date('Y-m-d H:i:s');

    $sql_update = "UPDATE $table SET date = '$date' WHERE username = '$username'";
    $result = mysqli_query($conn, $sql_update);
}

function login_sql($conn, $form_vars)
{
    //prepare data before insert into table
    // mysql_real_escape_string
    $username = trim(strip_tags($form_vars['uname']));
    $password = $form_vars['password'];

    //connect and select database
    $database = "userex";
    $sql = "USE $database";
    $result = mysqli_query($conn, $sql);

    //check if username and password exist
    $table = "users";
    $sql = "SELECT username, password, role FROM $table WHERE username = '$username'";
    $result = mysqli_query($conn, $sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();

        if (password_verify($password, $row['password'])) {
            $_SESSION["username"] = $username;
            $_SESSION["password"] = $password;
            $_SESSION["role"] = $row['role'];

            add_date($conn, $username);
        } else {
            echo "Username or password is incorrect";
        }
    } else {
        echo "Username or password does not exist";
    }

}


if (isset($_POST["submit"])) {

    $form_vars = [
        'uname' => '',
        'password' => ''
    ];

    $form_vars = fix_variable($form_vars);
    $validators = validate($form_vars);
    $order = isset($form_vars['order']) ? $form_vars['order'] : 'ASC';

    $general_message = validate_entire_form($validators);

    if ($general_message == "Login data successfully validated") {
        require_once 'conn.php';
        login_sql($conn, $form_vars);

        //Redirect to showMembers
        header("Location: http://localhost:63342/workspace/ex_ex/showMembers.php?order=" . $_POST['order']);
        exit();
    }

} else {
    require_once 'views/loginForm.php';
}
