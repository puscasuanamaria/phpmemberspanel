<?php
error_reporting(E_ALL & ~E_NOTICE);
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once 'views/header.php';
require_once 'db.php';

//get username from request
if (!empty($_SESSION['username'])) {
    $member = $_GET['username'];
} else {
    //Redirect to showMembers
    header("Location: http://localhost:63342/workspace/ex_ex/showMembers.php?order=ASC");
    exit();
}

//connect and select database
$database = "userex";
$sql = "USE $database";
$result = mysqli_query($conn, $sql);

//show user from table
$table = "users";

$sql = "SELECT * FROM $table  WHERE username = '$member'";
$result = mysqli_query($conn, $sql);

//prepare the data
$rows = [];

//prepare photo path
$target_dir = "./uploads/";
$extension = "";

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $photoName = $target_dir . $row['username'] . "." . $row['extension'];
    $row['photo'] = $photoName;
}

require_once 'views/showMemberDetailsTable.php';
