<?php
error_reporting(E_ALL & ~E_NOTICE);
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once 'db.php';

//get username from delete request
if (!empty($_SESSION['username']) && ($_SESSION['role'] == 'admin')) {
    $member = $_GET['username'];

} else {
    //Redirect to showMembers
    header("Location: http://localhost:63342/workspace/ex_ex/showMembers.php?order=ASC");
    exit();
}

//connect and select database
$database = "userex";
$sql = "USE $database";
$result = mysqli_query($conn, $sql);

//show user from table
$table = "users";

$sql = "DELETE FROM $table WHERE username = '$member'";
$result = mysqli_query($conn, $sql);

//Redirect to showMembers
header("Location: http://localhost:63342/workspace/ex_ex/showMembers.php?order=ASC");
exit();
