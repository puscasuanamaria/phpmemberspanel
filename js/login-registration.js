function validateFname(fname) {
    fname.style.border = "1px solid #DADADA";
    let fnameError = document.getElementById('fname_error');
    fnameError.style.color = "";
    fnameError.innerHTML = "";

    if (fname.value === '' || fname.value === null || fname.value === undefined) {
        fname.style.border = "1px solid red";
        let fnameError = document.getElementById('fname_error');
        fnameError.style.color = "#CCAAAD";
        fnameError.innerHTML = "Full name is required";
        fname.focus();
    } else if (fname.value.length < 3) {
        fname.style.border = "1px solid red";
        let fnameError = document.getElementById('fname_error');
        fnameError.style.color = "#CCAAAD";
        fnameError.innerHTML = "Full name must be bigger than 3 characters";
        fname.focus();
    }
}

function validateUname(uname) {
    uname.style.border = "1px solid #DADADA";
    let unameError = document.getElementById('uname_error');
    unameError.style.color = "";
    unameError.innerHTML = "";

    if (uname.value === '' || uname.value === null || uname.value === undefined) {
        uname.style.border = "1px solid red";
        let unameError = document.getElementById('uname_error');
        unameError.style.color = "#CCAAAD";
        unameError.innerHTML = "Username is required";
        uname.focus();
    } else if (uname.value.length < 3) {
        uname.style.border = "1px solid red";
        let unameError = document.getElementById('uname_error');
        unameError.style.color = "#CCAAAD";
        unameError.innerHTML = "Username must be bigger than 3 characters";
        uname.focus();
    } else {
        // let request = new XMLHttpRequest();
        // request.open("POST", "ajaxValidate.php", true);
        // request.setRequestHeader("Content-type", "application/json");
        // let str_json = JSON.stringify(uname.value);
        // request.send(str_json);
        //
        // let x = request.responseText;
        //
        // console.log(this.responseText);
        //
        // uname.style.border = "1px solid red";
        // let unameError = document.getElementById('uname_error');
        // unameError.style.color = "#CCAAAD";
        // unameError.innerHTML = "Username already exists";
        // uname.focus();
    }
}

function validateEmail(email) {
    email.style.border = "1px solid #DADADA";
    let emailError = document.getElementById('email_error');
    emailError.style.color = "";
    emailError.innerHTML = "";

    let emailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (email.value === '' || email.value === null || email.value === undefined) {
        email.style.border = "1px solid red";
        let emailError = document.getElementById('email_error');
        emailError.style.color = "#CCAAAD";
        emailError.innerHTML = "E-mail is required";
        email.focus();
    } else if (!email.value.match(emailformat)) {
        email.style.border = "1px solid red";
        let emailError = document.getElementById('email_error');
        emailError.style.color = "#CCAAAD";
        emailError.innerHTML = "You have entered an invalid email address!";
        email.focus();
    }
}

function validatePhone(phone) {
    phone.style.border = "1px solid #DADADA";
    let phoneError = document.getElementById('phone_error');
    phoneError.style.color = "";
    phoneError.innerHTML = "";

    let phoneformat = /^\+?([0-9]{2})\)?[ ]?([0-9]{4})[ ]?([0-9]{4})$/;

    if (phone.value === '' || phone.value === null || phone.value === undefined) {
        phone.style.border = "1px solid red";
        let phoneError = document.getElementById('phone_error');
        phoneError.style.color = "#CCAAAD";
        phoneError.innerHTML = "Phone is required";
        phone.focus();
    } else if (!phone.value.match(phoneformat)) {
        phone.style.border = "1px solid red";
        let phoneError = document.getElementById('phone_error');
        phoneError.style.color = "#CCAAAD";
        phoneError.innerHTML = "You have entered an invalid phone number!";
        phone.focus();
    }
}

function validatePassword(password) {
    password.style.border = "1px solid #DADADA";
    let passwordError = document.getElementById('password_error');
    passwordError.style.color = "";
    passwordError.innerHTML = "";

    let passwordformat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;

    if (password.value === '' || password.value === null || password.value === undefined) {
        password.style.border = "1px solid red";
        let passwordError = document.getElementById('password_error');
        passwordError.style.color = "#CCAAAD";
        passwordError.innerHTML = "Password is required";
        password.focus();
    } else if (password.value.length < 6) {
        password.style.border = "1px solid red";
        let passwordError = document.getElementById('password_error');
        passwordError.style.color = "#CCAAAD";
        passwordError.innerHTML = "Password must be bigger than 6 characters";
        password.focus();
    } else if (!password.value.match(passwordformat)) {
        password.style.border = "1px solid red";
        let passwordError = document.getElementById('password_error');
        passwordError.style.color = "#CCAAAD";
        passwordError.innerHTML = "Password must contain at least one numeric digit, one uppercase and one lowercase letter";
        password.focus();
    }
}

function validateRpassword(rpassword, password) {
    rpassword.style.border = "1px solid #DADADA";
    let rpasswordError = document.getElementById('rpassword_error');
    rpasswordError.style.color = "";
    rpasswordError.innerHTML = "";

    let rpasswordformat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;

    if (rpassword.value === '' || rpassword.value === null || rpassword.value === undefined) {
        rpassword.style.border = "1px solid red";
        let rpasswordError = document.getElementById('rpassword_error');
        rpasswordError.style.color = "#CCAAAD";
        rpasswordError.innerHTML = "Password is required";
        rpassword.focus();
    } else if (password.value !== rpassword.value) {
        rpassword.style.border = "1px solid red";
        let rpasswordError = document.getElementById('rpassword_error');
        rpasswordError.style.color = "#CCAAAD";
        rpasswordError.innerHTML = "Passwords are not equal";
        rpassword.focus();
    } else if (rpassword.value.length < 6) {
        rpassword.style.border = "1px solid red";
        let rpasswordError = document.getElementById('rpassword_error');
        rpasswordError.style.color = "#CCAAAD";
        rpasswordError.innerHTML = "Password must be bigger than 6 characters";
        rpassword.focus();
    } else if (!rpassword.value.match(rpasswordformat)) {
        rpassword.style.border = "1px solid red";
        let rpasswordError = document.getElementById('rpassword_error');
        rpasswordError.style.color = "#CCAAAD";
        rpasswordError.innerHTML = "Password must contain at least one numeric digit, one uppercase and one lowercase letter";
        rpassword.focus();
    }
}

function validateUpload(fileToUpload) {
    let uploadError = document.getElementById('upload_error');
    uploadError.style.color = "";
    uploadError.innerHTML = "";

    if (fileToUpload.value === '' || fileToUpload.value === null || fileToUpload.value === undefined) {

        uploadError.style.color = "#CCAAAD";
        uploadError.innerHTML = "Upload photo is required";
    }
}

function validateOldAndNewPassword(oldPassword, newPassword) {
    newPassword.style.border = "1px solid #DADADA";
    let passwordError = document.getElementById('new_password_error');
    newPassword.style.color = "";
    newPassword.innerHTML = "";

    let passwordformat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;

    if (newPassword.value === '' || newPassword.value === null || newPassword.value === undefined) {
        newPassword.style.border = "1px solid red";
        let passwordError = document.getElementById('new_password_error');
        passwordError.style.color = "#CCAAAD";
        passwordError.innerHTML = "Password is required";
        newPassword.focus();
    } else if (oldPassword.value === newPassword.value) {
        newPassword.style.border = "1px solid red";
        let passwordError = document.getElementById('new_password_error');
        passwordError.style.color = "#CCAAAD";
        passwordError.innerHTML = "Old password and new password are the same.";
        newPassword.focus();
    } else if (newPassword.value.length < 6) {
        newPassword.style.border = "1px solid red";
        let passwordError = document.getElementById('new_password_error');
        passwordError.style.color = "#CCAAAD";
        passwordError.innerHTML = "Password must be bigger than 6 characters";
        newPassword.focus();
    } else if (!newPassword.value.match(passwordformat)) {
        newPassword.style.border = "1px solid red";
        let passwordError = document.getElementById('new_password_error');
        passwordError.style.color = "#CCAAAD";
        passwordError.innerHTML = "Password must contain at least one numeric digit, one uppercase and one lowercase letter";
        newPassword.focus();
    }
}

function deleteUser(username){
    let conf = confirm("Are you sure you want to delete this user?");
    if (conf == true) {
        window.location = "http://localhost:63342/workspace/ex_ex/deleteMember.php?username=" + username;
    }
}

function validateSubject(subject) {
    subject.style.border = "1px solid #DADADA";
    let subjectError = document.getElementById('subject_error');
    subjectError.style.color = "";
    subjectError.innerHTML = "";

    if (subject.value === '' || subject.value === null || subject.value === undefined) {
        subject.style.border = "1px solid red";
        let subjectError = document.getElementById('subject_error');
        subjectError.style.color = "#CCAAAD";
        subjectError.innerHTML = "Subject is required";
        subject.focus();
    } else if (subject.value.length < 2) {
        subject.style.border = "1px solid red";
        let subjectError = document.getElementById('subject_error');
        subjectError.style.color = "#CCAAAD";
        subjectError.innerHTML = "Full name must be bigger than 2 characters";
        subject.focus();
    }
}

function validateMessage(message) {
    message.style.border = "1px solid #DADADA";
    let messageError = document.getElementById('message_error');
    messageError.style.color = "";
    messageError.innerHTML = "";

    if (message.value === '' || message.value === null || message.value === undefined) {
        message.style.border = "1px solid red";
        let messageError = document.getElementById('message_error');
        messageError.style.color = "#CCAAAD";
        messageError.innerHTML = "Message is required";
        message.focus();
    } else if (message.value.length < 2) {
        message.style.border = "1px solid red";
        let messageError = document.getElementById('message_error');
        messageError.style.color = "#CCAAAD";
        messageError.innerHTML = "Message must be bigger than 2 characters";
        message.focus();
    }
}