<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Show members</title>
    <script src="./js/login-registration.js"></script>
</head>
<body>

<?php
$isLogged = false;
if (!empty($_SESSION['username'])) {
    $isLogged = true;
}

$isAdmin = false;
if (!empty($_SESSION['role']) && ($_SESSION['role'] == 'admin')) {
    $isAdmin = true;
}
?>

<div id="first">
    <table id="show">
        <tr>
            <th>Name</th>

            <?php if ($isLogged) : ?>
                <th>Username</th>
                <th>More detalis</th>
                <th>Send email</th>
                <?php if ($isAdmin) : ?>
                    <th>Delete</th>
                <?php endif; ?>
            <?php endif; ?>
        </tr>

        <?php foreach ($rows as $row): ?>

            <tr>
                <td> <?php echo $row['name'] ?> </td>

                <?php if ($isLogged) : ?>
                    <td style="<?php echo (isset($rows_friends) && in_array($row['username'], $rows_friends)) ? 'color:red' : ''; ?>"> <?php echo $row['username'] ?> </td>
                    <td>
                        <a href="http://localhost:63342/workspace/ex_ex/showMemberDetails.php?username=<?php echo $row['username'] ?>">More
                            details</a>
                    </td>
                    <td>
                        <a href="http://localhost:63342/workspace/ex_ex/sendEmail.php?username=<?php echo $row['username'] ?>">Send
                            e-mail</a>
                    </td>
                    <?php if ($isAdmin) : ?>
                        <td>
                            <a href="#" onClick="deleteUser('<?php echo $row['username'] ?>');">Delete</a>
                        </td>
                    <?php endif; ?>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
</body>
</html>
