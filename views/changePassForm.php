<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Change pass form</title>
    <script src="./js/login-registration.js"></script>
</head>

<body>
<form id='login' name='login' action="changePass.php" method="post" accept-charset='UTF-8'>
    <fieldset id="first">

        <div>
            <?php
            $value = isset($_POST['oldpassword']) ? $_POST['oldpassword'] : '';
            $error = "";
            if (isset($validators) && ($validators['oldpassword']['message'] !== "")) {
                $error = $validators['oldpassword']['message'];
                $style = "border: 1px solid red";
            }
            ?>
            <label>Old password:</label>
            <input id="upassword" name="oldpassword" placeholder="Old password" type="password" value="<?php echo $value; ?>"
                   style="<?php echo $style; ?>" onchange="validatePassword(this)">
            <div id="password_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <div>
            <?php
            $value = isset($_POST['newpassword']) ? $_POST['newpassword'] : '';
            $error = "";
            if (isset($validators) && ($validators['newpassword']['message'] !== "")) {
                $error = $validators['newpassword']['message'];
                $style = "border: 1px solid red";
            }
            ?>
            <label>New password:</label>
            <input id="upassword" name="newpassword" placeholder="New password" type="password" value="<?php echo $value; ?>"
                   style="<?php echo $style; ?>" onchange="validateOldAndNewPassword(oldpassword,this)">
            <div id="new_password_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <div>
            <?php
            $value = isset($_POST['rnewpassword']) ? $_POST['rnewpassword'] : '';
            $error = "";
            if (isset($validators) && ($validators['rnewpassword']['message'] !== "")) {
                $error = $validators['rnewpassword']['message'] . "<br>";
                $style = "border: 1px solid red";
            }
            if (isset($validators) && ($validators['password_equal']['message'] !== "")) {
                $error = $validators['password_equal']['message'];
                $style = "border: 1px solid red";
            }
            ?>
            <label>Repeated password:</label>
            <input id="upassword" name="rnewpassword" placeholder="Repeat new password" type="password" value="<?php echo $value; ?>"
                   style="<?php echo $style; ?>" onchange="validateRpassword(this, newpassword)">
            <div id="rpassword_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <input id="submit" type="submit" name="submit" value="Change">
    </fieldset>
</form>

</body>
</html>
