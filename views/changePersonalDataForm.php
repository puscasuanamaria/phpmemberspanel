<!DOCTYPE html>
<html lang="en">
<head>
    <title>Registration Form</title>
    <meta content="noindex, nofollow" name="robots">
    <script src="./js/login-registration.js"></script>
</head>

<body>

<div><?php echo isset($general_message) ? $general_message : ''; ?> </div>

<form id='details' name='details' action="changePersonalData.php" method="post" enctype="multipart/form-data">
    <fieldset id="first">

        <label>Full Name:</label>
        <div>
            <?php
            if (isset($_POST["submit"])){
                $value = isset($_POST['fname']) ? $_POST['fname'] : '';
                $error = "";
                if (isset($validators) && ($validators['fname']['message'] !== "")) {
                    $error = $validators['fname']['message'];
                    $style = "border: 1px solid red";
                }
            } else {
                $error = "";
                $value =  $old_informations['fname'];
            }
            ?>
            <input id="fname" name="fname"
                   type="text" value="<?php echo $value; ?>"
                   style="<?php echo $style; ?>" onchange="validateFname(this)">
            <div id="fname_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <div>
            <?php
            if (isset($_POST["submit"])){
                $value = isset($_POST['uname']) ? $_POST['uname'] : '';
                $error = "";
                if (isset($validators) && ($validators['uname']['message'] !== "")) {
                    $error = $validators['uname']['message'];
                    $style = "border: 1px solid red";
                }
            } else {
                $error = "";
                $value = $old_informations['uname'];
            }
            ?>
            <label>Username: </label>
            <input id="uname" name="uname"
                   type="text" value="<?php echo $value; ?>"
                   style="<?php echo $style; ?>" onchange="validateUname(this)">
            <div id="uname_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <div>
            <?php
            if (isset($_POST["submit"])){
                $value = isset($_POST['email']) ? $_POST['email'] : '';
                $error = "";
                if (isset($validators) && ($validators['email']['message'] !== "")) {
                    $error = $validators['email']['message'];
                    $style = "border: 1px solid red";
                }
            } else {
                $error = "";
                $value = $old_informations['email'];
            }
            ?>
            <label>Email:</label>
            <input id="email" name="email"
                   placeholder="<?php echo isset($old_informations['email']) ? $old_informations['email'] : ""; ?>"
                   type="text" value="<?php echo $value; ?>"
                   style="<?php echo $style; ?>" onchange="validateEmail(this)">
            <div id="email_error"></div>
            <div class="error">
                <?php echo $error; ?>

            </div>
        </div>

        <div>
            <?php
            $value = isset($_POST['upload_img']) ? $_POST['upload_img'] : '';
            $error = "";
            if (isset($validators) && ($validators['upload_img']['message'] !== "")) {
                $error = $validators['upload_img']['message'];
            }
            ?>
            <label>Upload photo:</label>
            <input type="file" name="fileToUpload" id="fileToUpload" value="
        <?php echo $value; ?>" onchange="validateUpload(this)"><br>
            <div id="upload_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <div>
            <label>Gender:</label>
            <input class="gender" name="gender" type="radio"
                   value="male" <?php echo (isset($old_informations['gender']) && ($old_informations['gender'] == 'male')) ? "checked" : ""; ?>>Male
            <input class="gender" name="gender" type="radio"
                   value="female" <?php echo (isset($old_informations['gender']) && ($old_informations['gender'] == 'female')) ? "checked" : ""; ?>>Female<br>
        </div>

        <div>
            <label>Civil status:</label>
            <input class="married" name="civil" type="radio"
                   value="married" <?php echo (isset($old_informations['civil']) && ($old_informations['civil'] == 'married')) ? 'checked' : ""; ?>>Married
            <input class="single" name="civil" type="radio"
                   value="single" <?php echo (isset($old_informations['civil']) && ($old_informations['civil'] == 'single')) ? 'checked' : ""; ?>>Single
            <input class="divorced" name="civil" type="radio"
                   value="divorced" <?php echo (isset($old_informations['civil']) && ($old_informations['civil'] == 'divorced')) ? 'checked' : ""; ?>>Divorced<br>
        </div>

        <input id="submit" type="submit" name="submit" value="Change">
    </fieldset>
</form>

</body>
</html>



