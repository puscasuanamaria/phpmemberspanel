<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Show member details</title>

</head>
<body>

<div id="first">
    <table id="show">
        <tr>
            <th>Name</th>
            <th>Username</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Gender</th>
            <th>Civil status</th>
            <th>Last login</th>
            <th>Photo</th>
        </tr>


        <tr>
            <td> <?php echo (isset($row) && isset($row['name'])) ? $row['name'] : ""; ?> </td>
            <td> <?php echo (isset($row) && isset($row['username'])) ? $row['username'] : ""; ?> </td>
            <td> <?php echo (isset($row) && isset($row['email'])) ? $row['email'] : ""; ?> </td>
            <td> <?php echo (isset($row) && isset($row['phone_no'])) ? $row['phone_no'] : ""; ?> </td>
            <td> <?php echo (isset($row) && isset($row['gender'])) ? $row['gender'] : ""; ?> </td>
            <td> <?php echo (isset($row) && isset($row['civil_status'])) ? $row['civil_status'] : ""; ?> </td>
            <td> <?php echo (isset($row) && isset($row['date'])) ? $row['date'] : ""; ?> </td>
            <td>
                <img src="<?php echo (isset($row) && isset($row['photo'])) ? $row['photo'] : ""; ?>"
                     alt="<?php echo (isset($row) && isset($row['username'])) ? $row['username'] : ""; ?>"
                     style="width:60px;height:60px;">
            </td>
        </tr>

    </table>
</div>
</body>
</html>
