<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Header</title>

    <link rel="stylesheet" type="text/css" href="./css/login-registration.css">

</head>
<body>

<?php
$isLogged = false;
if (!empty($_SESSION['username'])) {
    $isLogged = true;
}

?>
<div id="meniu">
    <a href="./home.php" class="myButton">Home</a>
    <a href="./showMembers.php" class="myButton">Show members</a>

    <?php if ($isLogged) : ?>
        <a href="./selectFriends.php" class="myButton">Select friends</a>
    <?php endif; ?>

    <?php if (!$isLogged) : ?>
        <a href="./registration.php" class="myButton">Create account</a>
    <?php endif; ?>

    <?php if ($isLogged) : ?>
        <a href="changePass.php" class="myButton">Change password</a>
    <?php endif; ?>

    <?php if ($isLogged) : ?>
        <a href="changePersonalData.php" class="myButton">Edit details</a>
    <?php endif; ?>

    <?php if (!$isLogged) : ?>
        <a href="./login.php" class="myButton">Login</a>
    <?php endif; ?>

    <?php if ($isLogged) : ?>
        <a href="./logout.php" class="myButton">Logout</a>
    <?php endif; ?>
</div>


</body>
</html>