<!DOCTYPE html>
<html lang="en">
<head>
    <title>Registration Form</title>
    <meta content="noindex, nofollow" name="robots">
    <script src="./js/login-registration.js"></script>
</head>

<body>

<div><?php echo isset($general_message) ? $general_message : ''; ?> </div>

<form id='registration' name='registration' action="registration.php" method="post" enctype="multipart/form-data">
    <fieldset id="first">

        <label>Full Name:</label>
        <div>
            <?php
            $value = isset($_POST['fname']) ? $_POST['fname'] : '';
            $error = "";
            if (isset($validators) && ($validators['fname']['message'] !== "")) {
                $error = $validators['fname']['message'];
                $style = "border: 1px solid red";
            }
            ?>
            <input id="fname" name="fname" placeholder="Full Name" type="text" value="<?php echo $value; ?>"
                   style="<?php echo $style; ?>" onchange="validateFname(this)">
            <div id="fname_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <div>
            <?php
            $value = isset($_POST['uname']) ? $_POST['uname'] : '';
            $error = "";
            if (isset($validators) && ($validators['uname']['message'] !== "")) {
                $error = $validators['uname']['message'];
                $style = "border: 1px solid red";
            }
            ?>
            <label>Username: </label>
            <input id="uname" name="uname" placeholder="Username" type="text" value="<?php echo $value; ?>"
                   style="<?php echo $style; ?>" onchange="validateUname(this)">
            <div id="uname_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <div>
            <?php
            $value = isset($_POST['email']) ? $_POST['email'] : '';
            $error = "";
            if (isset($validators) && ($validators['email']['message'] !== "")) {
                $error = $validators['email']['message'];
                $style = "border: 1px solid red";
            }
            ?>
            <label>Email:</label>
            <input id="email" name="email" placeholder="Email" type="text" value="<?php echo $value; ?>"
                   style="<?php echo $style; ?>" onchange="validateEmail(this)">
            <div id="email_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <label>Phone number:</label>
        <div>
            <?php
            $value = isset($_POST['phone']) ? $_POST['phone'] : '';
            $error = "";
            if (isset($validators) && ($validators['phone']['message'] !== "")) {
                $error = $validators['phone']['message'];
                $style = "border: 1px solid red";
            }
            ?>
            <input id="phone" name="phone" placeholder="Phone number" type="text" value="<?php echo $value; ?>"
                   style="<?php echo $style; ?>" onchange="validatePhone(this)">
            <div id="phone_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <div>
            <?php
            $value = isset($_POST['password']) ? $_POST['password'] : '';
            $error = "";
            if (isset($validators) && ($validators['password']['message'] !== "")) {
                $error = $validators['password']['message'];
                $style = "border: 1px solid red";
            }
            ?>
            <label>New password:</label>
            <input id="password" name="password" placeholder="New password" type="password" value="<?php echo $value; ?>"
                   style="<?php echo $style; ?>" onchange="validatePassword(this)">
            <div id="password_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <div>
            <?php
            $value = isset($_POST['rpassword']) ? $_POST['rpassword'] : '';
            $error = "";
            if (isset($validators) && ($validators['rpassword']['message'] !== "")) {
                $error = $validators['rpassword']['message'] . "<br>";
                $style = "border: 1px solid red";
            }
            if (isset($validators) && ($validators['password_equal']['message'] !== "")) {
                $error = $validators['password_equal']['message'];
                $style = "border: 1px solid red";
            }
            ?>
            <label>Repeat password:</label>
            <input id="rpassword" name="rpassword" placeholder="Repeat password" type="password"
                   value="<?php echo $value; ?>" style="<?php echo $style; ?>" onchange="validateRpassword(this, password)">
            <div id="rpassword_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <div>
            <?php
            $value = isset($_POST['upload_img']) ? $_POST['upload_img'] : '';
            $error = "";
            if (isset($validators) && ($validators['upload_img']['message'] !== "")) {
                $error = $validators['upload_img']['message'];
            }
            ?>
            <label>Upload photo:</label>
            <input type="file" name="fileToUpload" id="fileToUpload" value="<?php echo $value; ?>" onchange="validateUpload(this)"><br>
            <div id="upload_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <div>
            <label>Gender:</label>
            <input class="gender" name="gender" type="radio" value="male">Male
            <input class="gender" name="gender" type="radio" value="female">Female<br>
        </div>

        <div>
            <label>Civil status:</label>
            <input class="married" name="civil" type="radio" value="married">Married
            <input class="single" name="civil" type="radio" value="single">Single
            <input class="divorced" name="civil" type="radio" value="divorced">Divorced<br>
        </div>



        <input id="submit" type="submit" name="submit" value="Continue">
    </fieldset>
</form>

</body>
</html>


