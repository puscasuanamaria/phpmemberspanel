<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Send message</title>
    <script src="./js/login-registration.js"></script>
</head>
<body>

<?php
$isLogged = false;
if (!empty($_SESSION['username'])) {
    $isLogged = true;
}

?>
<?php if ($isLogged) : ?>
    <form id='message' name='message' action="sendEmail.php" method="post" accept-charset='UTF-8'>

        <fieldset id="first">
            <?php
            if (isset($_POST["submit"])) {
                $value = isset($_POST['email']) ? $_POST['email'] : '';
                $error = "";
            } else {
                $error = "";
                $value = $userEmailToSend;
            }
            ?>
            <div>
                <input id="email" name="email" placeholder="Email" type="text" value="<?php echo $value; ?>"
            </div>

            <div>
                <?php
                $value = isset($_POST['subject']) ? $_POST['subject'] : '';
                $error = "";
                if (isset($validators) && ($validators['subject']['message'] !== "")) {
                    $error = $validators['subject']['message'];
                    $style = "border: 1px solid red";
                }
                ?>
                <label>Subject: </label>
                <input id="subject" name="subject" placeholder="subject" type="text" value="<?php echo $value; ?>"
                       style="<?php echo $style; ?>" onchange="validateSubject(this)">
                <div id="subject_error"></div>
                <div class="error">
                    <?php echo $error; ?>
                </div>
            </div>

            <div>
                <?php
                $value = isset($_POST['message']) ? $_POST['message'] : '';
                $error = "";
                if (isset($validators) && ($validators['message']['message'] !== "")) {
                    $error = $validators['message']['message'];
                    $style = "border: 1px solid red";
                }
                ?>
                <label>Message: </label>
                <textarea id="message" name="message" rows='15' cols='40'> <?php echo $value; ?></textarea><br/>

                <div id="message_error"></div>
                <div class="error">
                    <?php echo $error; ?>
                </div>
            </div>

            <input id="submit" type="submit" name="submit" value="Send">
        </fieldset>
    </form>

<?php endif; ?>
</body>
</html>