<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login Form</title>
    <script src="./js/login-registration.js"></script>
</head>

<body>
<form id='login' name='login' action="login.php" method="post" accept-charset='UTF-8'>
    <fieldset id="first">

        <div>
            <?php
            $value = isset($_POST['uname']) ? $_POST['uname'] : '';
            $error = "";
            if (isset($validators) && ($validators['uname']['message'] !== "")) {
                $error = $validators['uname']['message'];
                $style = "border: 1px solid red";
            }
            ?>
            <label>Username: </label>
            <input id="uname" name="uname" placeholder="Username" type="text" value="<?php echo $value; ?>"
                   style="<?php echo $style; ?>" onchange="validateUname(this)">
            <div id="uname_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <div>
            <?php
            $value = isset($_POST['password']) ? $_POST['password'] : '';
            $error = "";
            if (isset($validators) && ($validators['password']['message'] !== "")) {
                $error = $validators['password']['message'];
                $style = "border: 1px solid red";
            }
            ?>
            <label>Password:</label>
            <input id="upassword" name="password" placeholder="Password" type="password" value="<?php echo $value; ?>"
                   style="<?php echo $style; ?>" onchange="validatePassword(this)">
            <div id="password_error"></div>
            <div class="error">
                <?php echo $error; ?>
            </div>
        </div>

        <label>Order by name:</label>
        <input class="ascending" name="order" type="radio" value="ASC">Ascending
        <input class="descending" name="order" type="radio" value="DESC">Descending<br><br>

        <input id="submit" type="submit" name="submit" value="Continue">
    </fieldset>
</form>

</body>
</html>