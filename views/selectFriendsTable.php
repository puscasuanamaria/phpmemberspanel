<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Show members</title>
    <script src="./js/login-registration.js"></script>
</head>
<body>

<?php
$isLogged = false;
if (!empty($_SESSION['username'])) {
    $isLogged = true;
}

?>
<?php if ($isLogged) : ?>
<form id='selectFriends' name='selectFriends' action="selectFriends.php" method="post" accept-charset='UTF-8'>

    <div id="first">
        <table id="show">
            <tr>
                <th>Name</th>
                <th>Username</th>
                <th>Select</th>
            </tr>

            <?php foreach ($rows as $row): ?>
                <tr>
                    <td> <?php echo $row['name'] ?> </td>
                    <td> <?php echo $row['username'] ?> </td>
                    <td>
                        <input type="checkbox" name="selected[]" value="<?php echo $row['username'] ?>">
                    </td>


                </tr>
            <?php endforeach; ?>
        </table>
        <div style="text-align: center; margin: 20px;">
            <input id="submit" type="submit" name="submit" value="Select" >
        </div>

    </div>
</form>

<?php endif; ?>
</body>
</html>

