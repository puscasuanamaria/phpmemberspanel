<?php
error_reporting(E_ALL & ~E_NOTICE);
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once 'views/header.php';
require_once 'db.php';

//get order type from login form
if (!empty($_SESSION['username'])) {
    $orderOption = $_GET['order'];
} else {
    $orderOption = 'ASC';
}

//connect and select database
$database = "userex";
$sql = "USE $database";
$result = mysqli_query($conn, $sql);

//show all friends from table
if (!empty($_SESSION['username'])) {
    //show all from table
    $table = "users_friends";

    $sql = "SELECT friend_username FROM $table WHERE owner_username = '" . $_SESSION['username'] . "'";

    $result = mysqli_query($conn, $sql);
    $rows_friends = [];

    while ($row = mysqli_fetch_array($result)) {

        $rows_friends[] = $row['friend_username'];
    }
}


//show all from table
$table = "users";

$sql = "SELECT * FROM $table ORDER BY username $orderOption";
$result = mysqli_query($conn, $sql);

//prepare the data
$rows = [];

//prepare photo path
$target_dir = "./uploads/";
$extension = "";

while ($row = mysqli_fetch_array($result)) {

    $localData['name'] = $row['name'];
    $localData['username'] = $row['username'];
    $localData['email'] = $row['email'];
    $localData['gender'] = $row['gender'];
    $localData['civil_status'] = $row['civil_status'];
    $localData['date'] = $row['date'];

    $photoName = $target_dir . $row['username'] . "." . $row['extension'];
    $localData['photo'] = $photoName;

    $localData['role'] = $row['role'];

    $rows[] = $localData;
}

require_once 'views/showMembersTable.php';