<?php
require_once 'config/config.php';

$conn = mysqli_connect($servername, $username, $password, $database);

if (!$conn) {
    echo "Eroare: Nu a fost posibilă conectarea la MySQL." . PHP_EOL . "<br>";
    echo "Valoarea err: " . mysqli_connect_errno() . PHP_EOL . "<br>";
    exit;
}