<?php
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}
require_once 'views/header.php';
require_once 'db.php';

function validate_password($field)
{
    $status = true;
    $message = "";
    if ($field == "") {
        $status = false;
        $message = 'No password was entered';
    } else if (strlen($field) < 6) {
        $status = false;
        $message = 'Passwords must be at least 6 characters';
    } else if (!preg_match("/[a-z]/", $field) ||
        !preg_match("/[A-Z]/", $field) ||
        !preg_match("/[0-9]/", $field)) {
        $status = false;
        $message = 'Passwords require 1 each of a-z, A-Z and 0-9';
    }

    return [
        'status' => $status,
        'message' => $message
    ];
}

function validate_passwords_are_equal($password, $rpassword)
{
    $status = true;
    $message = "";
    if ($password !== $rpassword) {
        $status = false;
        $message = 'The passwords are not equal';
    }

    return [
        'status' => $status,
        'message' => $message
    ];
}

function fix_string($string)
{
    if (get_magic_quotes_gpc()) {
        $string = stripslashes($string);
    }
    return htmlentities($string);
}

function fix_variable($form_vars)
{
    if (isset($_POST['oldpassword']))
        $form_vars['oldpassword'] = fix_string($_POST['oldpassword']);
    if (isset($_POST['newpassword']))
        $form_vars['newpassword'] = fix_string($_POST['newpassword']);
    if (isset($_POST['rnewpassword']))
        $form_vars['rnewpassword'] = fix_string($_POST['rnewpassword']);
    return $form_vars;
}

function validate($form_vars)
{
    $validators['oldpassword'] = validate_password($form_vars['oldpassword']);
    $validators['newpassword'] = validate_password($form_vars['newpassword']);
    $validators['rnewpassword'] = validate_password($form_vars['rnewpassword']);
    $validators['password_equal'] = validate_passwords_are_equal($form_vars['newpassword'], $form_vars['rnewpassword']);

    return $validators;
}

function validate_entire_form($validators)
{
    $fail = $validators['oldpassword']['message'] .
        $validators['newpassword']['message'] .
        $validators['rnewpassword']['message'] .
        $validators['password_equal']['message'];

    if ($fail == '') {
        return "Change data successfully validated";
    } else {
        return "Change data not successfully validated";
    }
}

function change_pass_sql($conn, $form_vars){
    //prepare data before change into table
    // mysql_real_escape_string
    $oldpassword = $_SESSION['password'];
    $user = $_SESSION['username'];

    $criptednewpassword = password_hash($form_vars['newpassword'], PASSWORD_DEFAULT);


    //connect and select database
    $database = "userex";
    $sql = "USE $database";
    mysqli_query($conn, $sql);

    //check if old password exist in table
    $table = "users";

    $sql = "SELECT password FROM $table WHERE username = '$user'";
    $result = mysqli_query($conn, $sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();

        if (password_verify($form_vars['newpassword'], $row['password'])) {
          echo "Old password and new password are the same.";
        } else {
            $sql_update = "UPDATE $table SET password = '$criptednewpassword' WHERE username = '$user'";
            $result = mysqli_query($conn, $sql_update);

        }
    }  "Username does not exist";
}

if (isset($_POST["submit"])) {

    $form_vars = [
        'oldpassword' => '',
        'newpassword' => '',
        'rnewpassword' => ''
    ];

    $form_vars = fix_variable($form_vars);

    $validators = validate($form_vars);

    $general_message = validate_entire_form($validators);

    if ($general_message == "Change data successfully validated") {
        require_once 'conn.php';
        change_pass_sql($conn, $form_vars);

        //Redirect to logout
        header("Location: http://localhost:63342/workspace/ex_ex/logout.php");
        exit();
    }
} else {
    require_once 'views/changePassForm.php';
}