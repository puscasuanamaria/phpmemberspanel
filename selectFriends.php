<?php
error_reporting(E_ALL & ~E_NOTICE);
if(session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once 'views/header.php';
require_once 'db.php';

//connect and select database
$database = "userex";
$sql = "USE $database";
$result = mysqli_query($conn, $sql);

//show all from table
$table = "users";

$sql = "SELECT * FROM $table ORDER BY username ASC" ;
$result = mysqli_query($conn, $sql);

//prepare the data
$rows = [];


while ($row = mysqli_fetch_array($result)) {

    $localData['name'] = $row['name'];
    $localData['username'] = $row['username'];

    $rows[] = $localData;
}

if (isset($_POST["submit"])) {

    if(!empty($_POST['selected']) && (is_array($_POST['selected']))){

        //connect and select database
        $database = "userex";
        $sql = "USE $database";
        $result = mysqli_query($conn, $sql);

        $owner_username = $_SESSION['username'];
        $friends_username_array=$_POST['selected'];

        //prepare table
        $table = "users_friends";

        //delete all friends for owner username
        $sql = "DELETE FROM $table WHERE owner_username = '$owner_username'";
        $result = mysqli_query($conn, $sql);

        foreach ($friends_username_array as $friend ){

            $table = "users_friends";

            //insert into table - prepare statement
            $stmt = $conn->prepare("INSERT INTO $table (owner_username, friend_username)
                            VALUES (?, ?)");

            $stmt->bind_param("ss", $owner_username, $friend);
            $stmt->execute();
        }
    }
} else {
    require_once 'views/selectFriendsTable.php';
}


